
/*
 * Single method for managing state that
 * needs to persist through HTTP requests.
 */



var eatFacetsLib = {

  redrawUi: function($) {
    eatFacetsLib.collapseFacets($);
    if (eatFacetsLib.getCookie($.cookie) != null) {
      $("div.eat-facets")
        .eq(eatFacetsLib.getCookie($.cookie))
        .addClass("aria-expanded")
        .removeClass("aria-collapsed")
    }
  },

  collapseFacets: function($) {
    $("div.eat-facets")
      .removeClass("aria-expanded")
      .addClass("aria-collapsed");
  },

  setCookie: function(jqc, num) {
      if (typeof jqc == 'function') {
        jqc("eat-facets-expanded", num);
        return true;
      };
      return false;
  },

  removeCookieVal: function(jqc) {
      if (typeof jqc == 'function') {
        jqc("eat-facets-expanded", "null");
        return true;
      };
      return false;
  },

  getCookie: function(jqc) {
    if (typeof jqc == 'function') {
      return jqc("eat-facets-expanded");
    };
    return false;
  },

  // Helper method/s:
  isInteger: function(x) {
    return ((x % 1 === 0) && (x != null));
  }
}
