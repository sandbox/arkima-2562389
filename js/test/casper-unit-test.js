// Testing sprint22

var lib = eatFacetsLib;

/* Set up a mock function */
var jqc = function(name, num) {
  if (name === "eat-facets-expanded") {
    return lib.isInteger(0);
  };
  return num;
}

var jqrc = {
  removeCookie: function() {
  },
  getCookie: function() {
  }
};

// var jqrc = function(name) {
//   if (name === "eat-facets-expanded") {
//     return true;
//   };
//   return false;
// }

// Break out into 2 functions.
casper.test.begin('Set state should take a value and return the same value', 2, function(test) {
  var input = 0;
  lib.setCookie(jqc, input);
  test.assertTrue(lib.getCookie(jqc), input, "Values input and output values are the same.");
  console.log("Get", lib.getCookie(jqc));
  test.assertEquals(0, input, "Values input and output values are the same.");
  test.done();

});


casper.test.begin("Check that an integer is returned", 2, function(test) {

  var input = 0;
  var isIntInterogative = lib.isInteger(input);
  test.assertTrue(isIntInterogative, "input is a number, including 0");

  input = 1;
  var isIntInterogative = lib.isInteger(input);
  test.assertTrue(isIntInterogative, "input is a number, including 1");
  test.done();
});


casper.test.begin("Check that a cookie can be removed", 1, function(test) {
  lib.removeCookie(jqc);
  test.assertTrue(jqc.getCookie, "Values input and output values are the same.");
  test.done();
});
