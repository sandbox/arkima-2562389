(function($){

/**
 * Behavior for expanding and collapsing faceted search lists
 */

  Drupal.behaviors.eatFacets = {
    attach : function(context) {

      var getIndexNum, redrawUi, el;
      var lib = eatFacetsLib;

      /*
       * Scrub the slate, add back default classes,
       * and redraw.
       */
      $("div.eat-facets").addClass("aria-collapsed");
      lib.redrawUi($);

      /*
       * Work out the relative index number of a dom node.
       */
      getIndexNum = function(event) {
        var target = $(event.target);
        var parent = target.parent(".eat-facets");
        var num = parent.siblings().andSelf().index(parent);
        return num;
      }

    /*
     * Click event just sets state and calls the redraw UI function.
     */
    $(".eat-facets > h2").bind("click", function(event) {
      var target = $(event.target);
      var parent = target.parent(".eat-facets");
      if (parent.hasClass("aria-collapsed")) {
        lib.setCookie($.cookie, getIndexNum(event));
        lib.redrawUi($);
      } else {
        parent.removeClass("aria-expanded")
          .addClass("aria-collapsed");
        lib.removeCookieVal($.cookie);
        lib.redrawUi($);
      }
    });
  }
 };

})(jQuery);
