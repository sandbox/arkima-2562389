<?php

/**
 * @file
 *
 */

/**
 * Widget that renders facets as a list of clickable links.
 */
class EatFacets extends FacetapiWidgetLinks {
  /**
   * Renders the links.
   */
  public function execute() {
    $element = &$this->build[$this->facet['field alias']];

    // Sets each item's theme hook, builds item list.
    $this->setThemeHooks($element);
    $element = array(
      '#theme' => 'item_list',
      '#items' => $this->buildListItems($element),
      '#prefix' => '<div class="facet-collapsible-wrapper" id="facet-collapsible-' . str_replace('_', '-', $this->facet['field alias']) . '">',
      '#suffix' => '</div>',
      '#attributes' => array_merge_recursive($this->build['#attributes'], array('class' => array('eat-facets'))
      ),
    );
    $element['#attached'] ['js'][] = drupal_get_path ('module', 'eat_facets') . '/js/lib/eat_facets_lib.js';
    $element['#attached'] ['js'][] = drupal_get_path ('module', 'eat_facets') . '/js/eat_facets.js';
    $element['#attached'] ['css'][] = drupal_get_path ('module', 'eat_facets') . '/eat_facets.css';
  }
}
